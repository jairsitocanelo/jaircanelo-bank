public class cuenta1 {
    // bank account attributes
    static int accountNumber;
    static String name;
    double balance;
 
    // contructor method
    public cuenta1() {
        accountNumber = 7712;
        name = "tom";
        balance = 1900;
    }
 
    // deposit method
    public void deposit(double amount) {
        balance = balance + amount;
    }
 
    // withdraw method
    public void withdraw(double amount) {
        if (amount <= balance) {
            balance = balance - amount;
            // overdraft method
        } else if (amount > balance && balance > -100) {
            System.out.println("Overdraft limit reached.");
            balance = -100;
        }
    }
 
    // bank fee method
    public double bankFee() {
        double fee = balance * .05;
        balance = balance - fee;
        return balance;
    }
 
    // display method
    public void displayAccountDetails() {
        System.out.println("Account Number: " + accountNumber + "\n");
        System.out.println(name + ": $" + balance);
    }
 
    public static void main(String[] args) {
        cuenta1 tomsAccount = new cuenta1();
        tomsAccount.deposit(450);
        tomsAccount.bankFee();
        tomsAccount.withdraw(2200);
        tomsAccount.displayAccountDetails();
    }
}