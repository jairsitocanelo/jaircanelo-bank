public class cuenta {
    // bank account attributes
    static int accountNumber;
    static String name;
    double balance;
 
    // contructor method
    public cuenta() {
        accountNumber = 7712;
        name = "tom";
        balance = 1900;
    }
 
    // deposit method
    public void deposit(double amount) {
        balance = balance + amount;
    }
 
    // withdraw method
    public void withdraw(double amount) {
        if (amount <= balance) {
            balance = balance - amount;
        } else {
            System.out.print("Insufficient Funds");
        }
    }
 
    // bank fee method
    public double bankFee() {
        double fee = balance * .05;
        balance = balance - fee;
        return balance;
    }
 
    // display method
    public void displayAccountDetails() {
        System.out.println("Account Number: " + accountNumber + "\n");
        System.out.println(name + ": $" + balance);
    }
 
    // interest method
    public int accrueInterest() {
        int interest = (int) (balance * .05);
        balance = balance + interest;
        return (int) balance;
    }
 
    public static void main(String[] args) {
        cuenta tomsAccount = new cuenta();
        tomsAccount.deposit(450);
        tomsAccount.withdraw(2200);
        tomsAccount.accrueInterest();
        tomsAccount.bankFee();
        tomsAccount.displayAccountDetails();
    }
}